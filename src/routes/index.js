import React from "react";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Hooks } from "../cotainers/hooks";

import Welcome from "../cotainers/welcome";
import Forecast from "../cotainers/Forecast";
import Dashboard from "../cotainers/dashboard";

const RoutesConfigs = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Welcome />} />
        <Route exact path="/forecast" element={<Forecast />} />
        <Route exact path="/dashboard" element={<Dashboard />} />
      </Routes>
    </BrowserRouter>
  );
};
export default RoutesConfigs;
