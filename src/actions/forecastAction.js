import axios from "axios";
import { apiUrls } from "../apiurl";

export const getLocation = async (dispatch, lat, long) => {
  try {
    /** call the api get the bike locations show in next page after a button click or any actions */
    // const currentTemp = `${apiUrls.currentTemp}?latitude=${lat}&&longitude=${long}`;
    // console.log("url", currentTemp);
    // const response = await axios.get(
    //   currentTemp,

    //   { headers: { token: `Beare dunnny` } }
    // );
    // console.log("resp", response);
    const dummy = {
      cod: "200",
      message: 0.0179,
      cnt: 96,
      list: [
        {
          dt: 1596632400,
          main: {
            temp: 289.16,
            feels_like: 288.41,
            temp_min: 289.16,
            temp_max: 289.16,
            pressure: 1013,
            sea_level: 1013,
            grnd_level: 1010,
            humidity: 78,
            temp_kf: 0,
          },
          weather: [
            {
              id: 804,
              main: "Clouds",
              description: "overcast clouds",
              icon: "04n",
            },
          ],
          clouds: {
            all: 100,
          },
          wind: {
            speed: 2.03,
            deg: 252,
            gust: 5.46,
          },
          visibility: 10000,
          pop: 0.04,
          sys: {
            pod: "n",
          },
          dt_txt: "2020-08-05 13:00:00",
        },
      ],
    };
    dispatch({ type: "STORE_WEATHER_DATA", value: dummy });
  } catch (err) {
    console.log("error", err);
  }
};
