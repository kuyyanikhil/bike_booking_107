import axios from "axios";

export const welcomeLoad = async (dispatch, value) => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/users"
  );
  console.log("response", response);

  dispatch({ type: "WELCOME_LOAD", value: response });
};
