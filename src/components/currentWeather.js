import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Clouds from "../assets/Clouds.jpg";
import rainny from "../assets/rainny.jpg";

import sunny from "../assets/sunny.jpg";

class CurrentWeather extends React.Component {
  render() {
    console.log();
    var weatherData =
      this.props.welcome &&
      this.props.welcome.weatherData &&
      this.props.welcome.weatherData.list[0];
    var type = weatherData && weatherData.weather[0];
    console.log("type", type, weatherData);
    return (
      <Card sx={{}}>
        <CardMedia
          component="img"
          height="400rem"
          image={Clouds}
          alt="currentImg"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {type ? type.main : ""}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {type ? type.description : ""}
          </Typography>
        </CardContent>
        {/* <CardActions>
          <Button size="small">Share</Button>
          <Button size="small">Learn More</Button>
        </CardActions> */}
      </Card>
    );
  }
}
export default CurrentWeather;
