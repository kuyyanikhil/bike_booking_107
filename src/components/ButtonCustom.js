import React from "react";
import Button from "@mui/material/Button";

const ButtonCustom = ({ name }) => {
  return <Button>{name}</Button>;
};
export default ButtonCustom;
