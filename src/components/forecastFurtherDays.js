import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Temp", 159, 6.0, 24, 4.0),
  createData("Humidity", 237, 9.0, 37, 4.3),
  createData("rain probabilty", 262, 16.0, 24, 6.0),
  createData("High/low", 305, 3.7, 67, 4.3),
  createData("realfeel", 356, 16.0, 49, 3.9),
];

export default function ForecastFurtherDays() {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 20 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Dates</TableCell>
            <TableCell align="right">1</TableCell>
            <TableCell align="right">2</TableCell>
            <TableCell align="right">4</TableCell>
            <TableCell align="right">4</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
