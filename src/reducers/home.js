const initialState = {
  data: null,
  weatherData: null,
};
export const welcome = (state = initialState, action) => {
  console.log("state", action.type);
  switch (action.type) {
    case "WELCOME_LOAD":
      return {
        ...state,
        data: action.value,
      };
    case "STORE_WEATHER_DATA":
      return {
        ...state,
        weatherData: action.value,
      };
    default:
      return state;
  }
};
