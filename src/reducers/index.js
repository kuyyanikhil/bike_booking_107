import { combineReducers } from "redux";

import { welcome } from "./home";

const appReducer = combineReducers({ welcome });
export default appReducer;
