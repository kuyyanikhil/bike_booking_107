import React, { useEffect, useState } from 'react';
import { Map, Marker } from "pigeon-maps"
import axios from "axios";
import Forecast from './Forecast';


const Dashboard = () => {
  const [network,setNetwork] = useState(null);
  const [ stations,setStations] = useState(null);
  const [hue, setHue] = useState(0)
  const color = `hsl(${hue % 360}deg 39% 70%)`

  useEffect(() => {
    getNetworks().then((data) => {
          console.log('netowrk',data);
      setNetwork(data.networks[0]);
    });

  },[]);

  useEffect(() => {
    if (network) {
      getStationByNetwork(network.href).then((data) => {
        setStations(data?.network?.stations);
      });
    }
  },[network]);

  function selectRideDetails(station){
    console.log(station);
    setHue(hue+20);
  }

  return (
    <>
      <h2>Select the Ride</h2>
      <div>
        <Forecast />
      </div>
      <ul>
      <h4>{network?.name}</h4>
      <p>Stations: {stations?.length || 'loading...'} </p>
        <Map height={300} defaultCenter={[50.879, 4.6997]} defaultZoom={11}>
          { stations && Array.isArray(stations) && stations.length > 0 && stations.map((station) => {
            return (
              <Marker 
                key={station.id}
                width={100}
                anchor={[station.latitude, station.longitude]}
                color={color} 
                onClick={() => setHue(hue + 20)} 
              />
            )
          })}
        </Map>
      </ul>
    </>
  );
}

export default Dashboard;




async function getNetworks() {
  const URL = 'http://api.citybik.es/v2/networks'
    const response = await axios.get(`${URL}`);
    return response.data;
}


async function getStationByNetwork(href) {
  const URL = 'http://api.citybik.es/';
  const response = await axios.get(
    `${URL}${href}`
  );

  return response.data;
}