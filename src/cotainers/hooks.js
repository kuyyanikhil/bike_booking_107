import React,{useState,useEffect} from "react"

export const Hooks =()=>{
    let defaultArr = [2,4]
    let [myArr,setArr]=useState([])
    useEffect(()=>{
      setArr(defaultArr)
    },[])
    return(
        myArr.map((each,i)=><div key={i}>{each}</div>)
    )
}