import React from "react";
import { connect } from "react-redux";
import * as welcomeActions from "../actions/welcome";

class Welcome extends React.Component {
  render() {
    console.log(this.props.welcome);
    return (
      <div>
        welcome load
        <button
          onClick={() => welcomeActions.welcomeLoad(this.props.dispatch, 3)}
        >
          fire
        </button>
      </div>
    );
  }
  componentDidMount = () => {
    welcomeActions.welcomeLoad(this.props.dispatch, 6);
  };
}
const mapStateToProps = (state) => {
  return {
    welcome: state.welcome,
  };
};
export default connect(mapStateToProps)(Welcome);
