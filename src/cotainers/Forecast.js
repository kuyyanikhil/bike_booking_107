import React from "react";
import { connect } from "react-redux";
import * as forecastAction from "../actions/forecastAction";
import CurrentWeather from "../components/currentWeather";
import ForecastFurtherDays from "../components/forecastFurtherDays";
import ButtonCustom from "../components/ButtonCustom";
import Loader from "react-loader-spinner";
import { getTemp } from "../actions/forecastAction";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

class Forecast extends React.Component {
  render() {
    return (
      <>
        {/* <Loader
          type="Circles"
          color="#00BFFF"
          height={100}
          width={100}
          //3 secs
        /> */}
        <CurrentWeather {...this.props} forecastAction={forecastAction} />
        <ForecastFurtherDays />
        {/* <ButtonCustom name="show bikes" /> */}
      </>
    );
  }
  componentDidMount = () => {
    /**read location or ip */
    window.navigator.geolocation.getCurrentPosition((success) =>
      forecastAction.getLocation(
        this.props.dispatch,
        success.coords.latitude,
        success.coords.longitude
      )
    );
  };
}
const mapStateToProps = (state) => {
  return {
    welcome: state.welcome,
  };
};

export default connect(mapStateToProps)(Forecast);
